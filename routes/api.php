<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/facebook', 'FacebookController@initHook');
Route::post('/facebook', 'FacebookController@updateHook');
Route::get('/instagram', 'InstagramController@initHook');
Route::post('/instagram', 'InstagramController@updateHook');
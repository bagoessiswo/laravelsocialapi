<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Instagram;
use Illuminate\Http\Request;

class InstagramController extends Controller
{
    public function index(){
        // $posts = Instagram::orderBy('created_at','DESC')->get();
        $client = new Client([
            'base_uri' => 'https://api.instagram.com/v1/',
           
            'timeout'  => 2.0,
        ]);
        $response = $client->get('https://api.instagram.com/v1/users/self/media/recent?access_token=35946142.ee8a18c.d18181d90be44c068fe64ed678f90c64');
       
        $posts = json_decode((string) $response->getBody(),true)['data'];
        return view('instagram')->with('posts',$posts);
    }

    public function initHook(Request $request){
        if($request->hub_mode == "subscribe" && $request->hub_verify_token == "verifytoken123"){
            return response($request->hub_challange);
        }
    }

    public function updateHook(Request $request){
        
        $body = $request->getContent();
        dump($body);
        

        return response()->json($body,200);
    }
}

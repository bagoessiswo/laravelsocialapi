<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $tweets = Tweet::orderBy('created_at','DESC')->get();

        return view('welcome')->with('tweets', $tweets);
    }
}

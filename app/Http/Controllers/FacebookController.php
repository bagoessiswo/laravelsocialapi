<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FacebookController extends Controller
{
    public function initHook(Request $request){
        if($request->hub_mode == "subscribe" && $request->hub_verify_token == "verifytoken123"){
            return response($request->hub_challange);
        }
    }

    public function updateHook(Request $request){
        $xHeader = $request->header('X-Hub-Signature');
        $body = $request->getContent();
        $signature = 'sha1=' . hash_hmac('sha1', $body, env('FACEBOOK_SECRET_API'));

        if($xHeader!=$signature){
            return response()->status(401);
        }

        

        return response()->json($body,200);
    }
}

<?php

namespace App\Console\Commands;

use TwitterStreamingApi;
use App\Tweet;
use Illuminate\Console\Command;

class ListenTwitterApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen Twitter API Stream';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // '3112363021'
        // 92017672 gus
        TwitterStreamingApi::publicStream()
        ->whenTweets('3112363021', function(array $tweet){
            Tweet::create([
                'text' => $tweet['text'],
                'img_url' => $tweet['entities']['media'][0]['media_url'],
            ]);
                // dump($tweet);
        })
        ->startListening();
    }
}

{{dump($posts)}}
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #101010;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .panel > panel-heading{
                color: #101010 !important;
            }
        </style>
    </head>
    <body>
       

            <div class="container">
            @foreach($posts as $post)
            <br>
            <div class="panel panel-default">
            <div class="panel-heading">{{$post['caption']['text']}}</div>
            <div class="panel-body">
            @if($post['type']=="video")
            <video width="320" height="240" controls>
            <source src="{{$post['videos']['standard_resolution']['url']}}" type="video/mp4">
            Your browser does not support the video tag.
            </video>
            @elseif($post['type']=="image")
            <img src="{{$post['images']['standard_resolution']['url']}}" style="width:30%;" alt="">
            @endif
            </div>
            </div>
            <br>
            <hr>
            <br>
           
            @endforeach
            </div>
    </body>
</html>